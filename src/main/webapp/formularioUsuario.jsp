<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: batoi
  Date: 11/5/21
  Time: 16:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Crear Usuario</title>
</head>
<body>
<a href="index.jsp">Volver</a>
<h1>Crear Usuario</h1>
<form id="userForm" method="post" action="user-add">
    <label> Nombre <input type="text" name="firstName"/></label>
    <br>
    <label> Apellido <input type="text" name="lastName"/></label>
    <br>
    <label> Email <input type="email" name="email"/></label>
    <br>
    <label> Teléfono <input type="number" name="phoneNumber"/></label>
    <br>
    <label> Contraseña <input type="password" name="password"/></label>
    <br>
    <label> Fecha nacimiento <input type="date" name="birthday"/></label>
    <br>
    <label for="empresas">Seleccione una empresa:</label>
    <select name="id_enterprise" id="empresas">
        <option value="0">Selecciona una empresa</option>
        <% ArrayList<String> idEnterprises = (ArrayList<String>) request.getAttribute("idEnterprises");
        ArrayList<String> nameEnterprises = (ArrayList<String>) request.getAttribute("nameEnterprises");%>%>
        <% for (int i = 0; i < idEnterprises.size(); i++) { %>
            <option value=<%=idEnterprises.get(i)%>><%=nameEnterprises.get(i)%></option>
        <%}%>
    </select>
    <input type="submit" name = "action" value="Crear">
</form>
</body>
</html>
