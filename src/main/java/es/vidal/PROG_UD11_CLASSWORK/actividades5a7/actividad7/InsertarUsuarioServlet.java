package es.vidal.PROG_UD11_CLASSWORK.actividades5a7.actividad7;

import es.vidal.PROG_UD11_CLASSWORK.MySQLConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

@WebServlet(name = "InsertarUsuarioServlet", value = "/user-add")
public class InsertarUsuarioServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MySQLConnection mySQLConnection = new MySQLConnection("172.16.225.173", "crm_db"
                , "batoi", "1234");
        Connection connection = mySQLConnection.getConnection();
        String sql = "SELECT * FROM Enterprise";
        ArrayList<String> idEmpresas = new ArrayList<>();
        ArrayList<String> nombresEmpresas = new ArrayList<>();
        try (
                Statement statement = connection.createStatement()
        )
        {
            ResultSet empresas = statement.executeQuery(sql);
            while (empresas.next()){
                idEmpresas.add(empresas.getString("id"));
                nombresEmpresas.add(empresas.getString("name"));
            }
            request.setAttribute("idEnterprises", idEmpresas);
            request.setAttribute("nameEnterprises", nombresEmpresas);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("/formularioUsuario.jsp").include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MySQLConnection mySQLConnection = new MySQLConnection("172.16.225.173", "crm_db"
                , "batoi", "1234");

        PrintWriter out = response.getWriter();
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phoneNumber");
        String password = request.getParameter("password");
        int active = 1;
        LocalDateTime createdOn = LocalDateTime.now();
        String createdOnTime = Timestamp.valueOf(createdOn).toString();
        String locale = "es_ES";
        String idEnterprise = request.getParameter("id_enterprise");
        LocalDate birthday = LocalDate.parse(request.getParameter("birthday"));
        String birhdayString = Date.valueOf(birthday).toString();

        String sql = "INSERT INTO User (firstName, lastName, email, phoneNumber, password, active, createdOn, lastLogin," +
                "locale, idEnterprise, birthday) VALUES ('" + firstName + "','" + lastName + "','" + email + "','" + phoneNumber + "'," +
                "'" + password + "','" + active +"','" + createdOnTime + "'," + null + ",'" + locale + "','" + idEnterprise +
                "','"+ birhdayString + "')";
        Connection connection = mySQLConnection.getConnection();

        try (
                Statement statement = connection.createStatement()
        )
        {
            int affectedRows = statement.executeUpdate(sql);
            out.println("<html><body>");
            out.println("<h1>Datos usuario </h1>");
            out.println("<b>Nombre: </b>" + firstName + "<br>");
            out.println("<b>Apellido: </b>" + lastName + "<br>");
            out.println("<b>Email: </b>" + email +"<br>");
            out.println("<b>Telefono: </b>" + phoneNumber +"<br>");
            out.println("<b>Password: </b>" + password + "<br>");
            out.println("<b>Cumpleaños: </b>" + birhdayString +"<br>");
            out.println("Filas modificadas: " + affectedRows);
            out.println("<html><body>");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
