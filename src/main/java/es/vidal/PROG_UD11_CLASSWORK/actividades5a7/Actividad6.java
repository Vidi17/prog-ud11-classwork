package es.vidal.PROG_UD11_CLASSWORK.actividades5a7;

import es.vidal.PROG_UD11_CLASSWORK.MySQLConnection;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Actividad6 {

    public static void main(String[] args) {

        modificarDatos();

    }

    public static void modificarDatos(){
        MySQLConnection mySQLConnection = new MySQLConnection("172.16.225.173", "crm_db"
                , "batoi", "1234");

        String sql = "UPDATE Enterprise SET name = 'Plásticos ALCO', address = 'Polígono industrial Cotes Altes'" +
                ", city = 'Alcoy', province = 'Alicante' WHERE id = 4";
        String sql2 = "UPDATE Enterprise SET name = 'Informática Alcoy', address = 'Polígono Industrial Santiago Payá'" +
                ", city = 'Alcoy', province = 'Alicante' WHERE id = 5";

        Connection connection = mySQLConnection.getConnection();

        try (
                Statement statement = connection.createStatement();
        )
        {
            int affectedRows = statement.executeUpdate(sql);
            int affectedRows2 = statement.executeUpdate(sql2);
            System.out.println("Filas modificadas: " + affectedRows + affectedRows2);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
