package es.vidal.PROG_UD11_CLASSWORK.actividades5a7;

import es.vidal.PROG_UD11_CLASSWORK.MySQLConnection;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class Actividad5 {

    public static void main(String[] args) {
        insertarDatos();

    }

    public static void insertarDatos(){
        MySQLConnection mySQLConnection = new MySQLConnection("172.16.225.173", "crm_db"
                , "batoi", "1234");

        String sql = "INSERT INTO Enterprise (name, address, city, province, country, Locale, nif, status)" +
                "VALUES ('IES Pare Victoria', 'Carrer La Rambla, 1','Alcoi','Alicante','ES','es_ES', '21343123L', " +
                "1), ('CIPFPBatoi', 'Carrer Societat Unió Musical, 8','Alcoi','Alicante','ES','es_ES', '21546342L', " +
                "1)";
        Connection connection = mySQLConnection.getConnection();

        try (
                Statement statement = connection.createStatement();
        )
        {
            int affectedRows = statement.executeUpdate(sql);
            System.out.println("Filas modificadas: " + affectedRows);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
