package es.vidal.PROG_UD11_CLASSWORK.actividad3;

import es.vidal.PROG_UD11_CLASSWORK.MySQLConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Actividad3 {

    public static void main(String[] args) {

        System.out.println("1 - Imprimir por pantalla el nombre, correo electrónico y teléfono de todos los usuarios.");
        mostrarNombreEmailTelf();
        System.out.println("2 - Imprimir por pantalla el nombre, dirección y ciudad de todas las empresas.");
        mostrarNombreDireccionEmpresa();
        System.out.println("3 - Imprimir por pantalla el nombre, apellidos y empresa de todos los usuarios que\n" +
                "pertenezcan a empresas con identificador 1 o 2.");
        mostrarNombreApellidosEmpresa();

    }

    public static void mostrarNombreEmailTelf(){
        MySQLConnection database = new MySQLConnection("172.16.225.173", "crm_db"
                , "batoi", "1234");

        String sql = "SELECT * FROM User";
        Connection connection = database.getConnection();
        try (
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(sql);
        )
        {
            while (rs.next()) {
                System.out.println("------------------------");
                System.out.println("Nombre: " + rs.getString("firstName"));
                System.out.println("Email: " + rs.getString("email"));
                System.out.println("Teléfono: " + rs.getString("phoneNumber"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void mostrarNombreDireccionEmpresa(){
        MySQLConnection database = new MySQLConnection("172.16.225.173", "crm_db"
                , "batoi", "1234");

        String sql = "SELECT * FROM Enterprise";
        Connection connection = database.getConnection();
        try (
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(sql);
        )
        {
            while (rs.next()) {
                System.out.println("------------------------");
                System.out.println("Nombre: " + rs.getString("name"));
                System.out.println("Dirección: " + rs.getString("address"));
                System.out.println("Ciudad: " + rs.getString("city"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void mostrarNombreApellidosEmpresa(){
        MySQLConnection database = new MySQLConnection("172.16.225.173", "crm_db"
                , "batoi", "1234");

        String sql = "SELECT * FROM User u INNER JOIN Enterprise e ON u.idEnterprise = e.id " +
                "WHERE idEnterprise IN (1,2) ";
        Connection connection = database.getConnection();
        try (
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(sql);
        )
        {
            while (rs.next()) {
                System.out.println("------------------------");
                System.out.println("Nombre: " + rs.getString("firstName"));
                System.out.println("Apellidos: " + rs.getString("lastName"));
                System.out.println("Empresa: " + rs.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
