package es.vidal.PROG_UD11_CLASSWORK.actividades1_2;

import java.sql.Connection;
import java.sql.DriverManager;

public class Actividad1 {
    public static void main(String[] args) {

        try {
            String dbURL = "jdbc:mysql://172.16.225.173/crm_db?serverTimezone=UTC&allowPublicKeyRetrieval=true";
            Connection con = DriverManager.getConnection(dbURL, "batoi","1234");
            System.out.println("La conexión se ha establecido con éxito");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
