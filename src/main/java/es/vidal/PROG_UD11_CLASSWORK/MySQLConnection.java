package es.vidal.PROG_UD11_CLASSWORK;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection {

    private String ip;

    private String db;

    private String user;

    private String password;

    private Connection connection;

    public MySQLConnection(String ip, String db, String user, String password) {
        this.ip = ip;
        this.db = db;
        this.user = user;
        this.password = password;
    }

    public Connection getConnection(){
        if (connection == null){
            registerConnection();
            try {
                String dbURL = "jdbc:mysql://" + ip + "/" + db + "?serverTimezone=UTC&allowPublicKeyRetrieval=true";
                connection = DriverManager.getConnection(dbURL, user, password);
                System.out.println("La conexión se ha establecido con éxito");
            }catch (SQLException exception){
                exception.getStackTrace();
            }
        }
        return connection;
    }

    private void registerConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error al registrar el driver de MySQL: " + ex);
        }
    }
}
